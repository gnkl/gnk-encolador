/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.hl7.dao.impl;

import java.util.List;
import mx.gnkl.hl7.dao.MensajeHl7Dao;
import mx.gnkl.hl7.model.MensajeHl7;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository
public class MensajeHl7DaoImpl implements MensajeHl7Dao {

    @Autowired
    private SessionFactory sessionFactory;
        
    public void insertMensaje(MensajeHl7 mensaje) {
        getSessionFactory().getCurrentSession().saveOrUpdate(mensaje);
    }

    public MensajeHl7 getMensajeById(int mensajeId) {
        return (MensajeHl7) getSessionFactory().getCurrentSession().get(MensajeHl7.class, mensajeId);
    }

    public List<MensajeHl7> getMensajeByStatus(int status) {
        Query query = getSessionFactory().getCurrentSession().createQuery("from MensajeHl7 where status = :status");
        query.setParameter("status", status);
        return query.list();
    }

    public List<MensajeHl7> getMensajes() {
        Query query = getSessionFactory().getCurrentSession().createQuery("from MensajeHl7");
        return query.list();        
    }

    /**
     * @return the sessionFactory
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * @param sessionFactory the sessionFactory to set
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
}
