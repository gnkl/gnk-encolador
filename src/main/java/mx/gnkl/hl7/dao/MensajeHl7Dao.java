/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.hl7.dao;

import java.util.List;
import mx.gnkl.hl7.model.MensajeHl7;

/**
 *
 * @author jmejia
 */
public interface MensajeHl7Dao {
    
	void insertMensaje(MensajeHl7 mensaje);
	
	MensajeHl7 getMensajeById(int mensajeId);
	
	List<MensajeHl7> getMensajeByStatus(int status);
	
	List<MensajeHl7> getMensajes();    
    
}
