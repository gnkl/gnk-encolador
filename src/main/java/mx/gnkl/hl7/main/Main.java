package mx.gnkl.hl7.main;

import java.util.List;
import mx.gnkl.hl7.model.MensajeHl7;
import mx.gnkl.hl7.service.MensajeHl7Service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main 
{
    public static void main( String[] args )
    {
    	ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
    	MensajeHl7Service serviceHl7 = (MensajeHl7Service) ctx.getBean(MensajeHl7Service.class);
        List<MensajeHl7> result = serviceHl7.getAllMensajeHl7ByStatus(1);
        System.out.println(result);
        if(!result.isEmpty()){
            for(MensajeHl7 msj: result ){
                if(serviceHl7.sendDatatoSocket(msj)){
                    msj.setStatus(1);
                    serviceHl7.insertMensajeHl7(msj);
                }
                //serviceHl7.insertMensajeHl7(msj);
            }
        }

    }
}
