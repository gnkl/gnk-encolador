/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.hl7.service;

import java.util.List;
import mx.gnkl.hl7.model.MensajeHl7;

/**
 *
 * @author jmejia
 */
public interface MensajeHl7Service {
	void insertMensajeHl7(MensajeHl7 mensaje);
	
	MensajeHl7 getMensajeHl7ById(int mensajeId);
	
	public List<MensajeHl7> getAllMensajeHl7ByStatus(int status);
	
	List<MensajeHl7> getAllMensajeHl7();    
        
        boolean verifyIfCanConnectToSocket(String host, int port);
        
        public boolean sendDatatoSocket(MensajeHl7 messageAck);
}
