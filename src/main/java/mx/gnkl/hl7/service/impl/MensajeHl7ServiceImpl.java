/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.hl7.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import mx.gnkl.hl7.dao.MensajeHl7Dao;
import mx.gnkl.hl7.model.MensajeHl7;
import mx.gnkl.hl7.service.MensajeHl7Service;
import mx.gnkl.hl7.utils.MensajeHl7Util;
import org.apache.commons.net.telnet.TelnetClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jmejia
 */
@Service
public class MensajeHl7ServiceImpl implements MensajeHl7Service{
    
    @Autowired
    private MensajeHl7Dao mensajeDAO;

    @Autowired
    private MensajeHl7Util utilHl7;
    
    @Override
    @Transactional
    public void insertMensajeHl7(MensajeHl7 mensaje) {
        getMensajeDAO().insertMensaje(mensaje);
    }

    @Override
    @Transactional    
    public MensajeHl7 getMensajeHl7ById(int mensajeId) {
        return getMensajeDAO().getMensajeById(mensajeId);
    }

    @Override
    @Transactional    
    public List<MensajeHl7> getAllMensajeHl7ByStatus(int status) {
        return getMensajeDAO().getMensajeByStatus(status);
    }

    @Override
    @Transactional    
    public List<MensajeHl7> getAllMensajeHl7() {
        return getMensajeDAO().getMensajes();
    }

    public boolean verifyIfCanConnectToSocket(String host, int port) {
        TelnetClient telnetClient = new TelnetClient();
        boolean isConnect=false;
        try {
            telnetClient.connect(host, port);
            telnetClient.disconnect();
            isConnect = true;
        } catch (ConnectException ce) {
            isConnect = false;
            ce.printStackTrace();
        } catch (UnknownHostException e) {
            isConnect = false;
            e.printStackTrace();
        } catch (IOException e) {
            isConnect = false;
            e.printStackTrace();
        }        
        return isConnect;
    }
    
    public boolean sendDatatoSocket(MensajeHl7 messageAck){
        Socket socket = null; 
        boolean messageIsSend=false;
        try {
            
            if(verifyIfCanConnectToSocket(getUtilHl7().getHost(), getUtilHl7().getPort())){
                
                InetAddress address = InetAddress.getByName(getUtilHl7().getHost());
                socket = new Socket(address, getUtilHl7().getPort());

                //Send the message to the server
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);

                bw.write(messageAck.getMensaje());
                bw.flush();
                System.out.println("Message sent to the server : " + messageAck);
                
//                messageAck.setStatus(1);
//                getMensajeDAO().insertMensaje(messageAck);

                //Get the return message from the server
//                InputStream is = socket.getInputStream();
//                InputStreamReader isr = new InputStreamReader(is);
//                BufferedReader br = new BufferedReader(isr);
//                String message = br.readLine();
//                System.out.println("Message received from the server : " + message);  
                messageIsSend=true;
                os.close();
//                is.close();
//                isr.close();
                osw.close();
            } else {
                System.out.println("Not send ack message");
                messageIsSend=false;

            }
        } catch (Exception exception) {
            exception.printStackTrace();
            messageIsSend=false;
        } finally {
            //Closing the socket
            try {
                if(socket!=null){
                    socket.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return messageIsSend;
    }    

    /**
     * @return the mensajeDAO
     */
    public MensajeHl7Dao getMensajeDAO() {
        return mensajeDAO;
    }

    /**
     * @param mensajeDAO the mensajeDAO to set
     */
    public void setMensajeDAO(MensajeHl7Dao mensajeDAO) {
        this.mensajeDAO = mensajeDAO;
    }

    /**
     * @return the utilHl7
     */
    public MensajeHl7Util getUtilHl7() {
        return utilHl7;
    }

    /**
     * @param utilHl7 the utilHl7 to set
     */
    public void setUtilHl7(MensajeHl7Util utilHl7) {
        this.utilHl7 = utilHl7;
    }
    
}
